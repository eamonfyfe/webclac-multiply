//import multiply from '../multiply';
var expect  = require('chai').expect;
var multiply = require('../multiply');

it('Multiply test', function () {
    let x = 13;
    let y = 10;
    const z = 130;
    expect(multiply.multiply(x,y)).to.equal(z);
})
