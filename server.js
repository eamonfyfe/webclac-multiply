var multiply = require('./multiply');

const express = require('express');

const PORT = 80;
const HOST = '0.0.0.0';

//let multi = require('./multiply');

const app = express();
app.get('/', (req,res) => {

    let output = {
        'error': false,
        'string': '',
        'answer': 0
    };

    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin', '*')
    var x = req.query.x;
    var y = req.query.y;
    var answer = multiply(x,y);

    output.string = x + '*' + y + '=' + answer;
    output.answer = answer;

    res.end(JSON.stringify(output));
});

app.listen(PORT, HOST);
